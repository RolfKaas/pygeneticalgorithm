# -*- coding: utf-8 -*-
''' A simple implementation of a genetic algorithm in Python '''

__author__ = "Christian Brinch"
__copyright__ = "Copyright 2018-2019"
__credits__ = ["Christian Brinch"]
__license__ = "AFL 3.0"
__version__ = "1.0"
__maintainer__ = "Christian Brinch"
__email__ = "cbri@dtu.dk"

from time import sleep
import copy as cp
import itertools as it
import random
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import deap.creator
import deap.base
import deap.tools
import deap.algorithms

# Parameters for the run
SPRITE = 'mario'
GENE_LENGTH = 1  # Used for precision when genes are floating points
MUTATION_RATE = 0.02
POPSIZE = 500
GENERATIONS = 1500

if SPRITE == 'ninja':
    BITMAP = np.array([
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1]
    ])
    COLORS = 'gray_r'
    CHOICES = [0, 1]
elif SPRITE == 'mario':
    BITMAP = np.array([
        [0, 0, 0, 3, 3, 3, 3, 3, 0, 0, 0, 0],
        [0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0],
        [0, 0, 2, 2, 2, 1, 1, 6, 1, 0, 0, 0],
        [0, 2, 1, 2, 1, 1, 1, 6, 1, 1, 1, 0],
        [0, 2, 1, 2, 2, 1, 1, 1, 6, 1, 1, 1],
        [0, 2, 2, 1, 1, 1, 1, 6, 6, 6, 6, 0],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 5, 3, 3, 5, 0, 0, 0, 0],
        [0, 3, 3, 3, 5, 3, 3, 5, 3, 3, 3, 0],
        [3, 3, 3, 3, 5, 5, 5, 5, 3, 3, 3, 3],
        [1, 1, 3, 5, 4, 5, 5, 4, 5, 3, 1, 1],
        [1, 1, 1, 5, 5, 5, 5, 5, 5, 1, 1, 1],
        [1, 1, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1],
        [0, 0, 5, 5, 5, 0, 0, 5, 5, 5, 0, 0],
        [0, 2, 2, 2, 0, 0, 0, 0, 2, 2, 2, 0],
        [2, 2, 2, 2, 0, 0, 0, 0, 2, 2, 2, 2]
    ])
    CHOICES = [0, 1, 2, 3, 4, 5, 6]
    COLORS = colors.ListedColormap(
        ['white', 'wheat', 'sienna', 'red', 'yellow', 'blue', 'black'])

N_GENES = len(BITMAP.ravel())

deap.creator.create("FitnessMax", deap.base.Fitness, weights=(1.0,))
deap.creator.create("Individual", list, fitness=deap.creator.FitnessMax)

toolbox = deap.base.Toolbox()

toolbox.register("random_bit", random.choice, CHOICES)
toolbox.register("individual", deap.tools.initRepeat, deap.creator.Individual,
                 toolbox.random_bit, n=N_GENES)

toolbox.register("population", deap.tools.initRepeat, list, toolbox.individual)


# Note that the function return value is expressed as a one element list of
# floats, as required by the FitnessMax weigths property defined previously
# as (1.0, )
def evaluateInd(individual):
    ''' Calculate the cost function for an individual '''
    fitness = 1.0 / (sum([pow(individual[i] - BITMAP.ravel()[i], 2)
                     for i, _ in enumerate(BITMAP.ravel())]) + 1e-30)
    return (fitness,)


def myMutation(individual):
    ''' Introduce a random mutation '''
    marker = np.random.randint(0, len(individual))
    individual[marker] = np.random.randint(0, np.max(BITMAP) + 1)
    return (individual,)


toolbox.register("mate", deap.tools.cxTwoPoint)
toolbox.register("select", deap.tools.selBest)
toolbox.register("evaluate", evaluateInd)
toolbox.register("mutate", myMutation)


def express(individual):
    ''' Reshape genome into a 2D array '''
    return np.reshape(individual, np.shape(BITMAP))


def main():
    ''' First define a plot window, then enter main loop '''

    fig = plt.figure(1)
    plt.clf()
    axis = [fig.add_subplot(2, 3, c) for c in [1, 2, 3]]
    axis.append(fig.add_subplot(2, 3, 5))
    for axe in axis[:-1]:
        axe.set_xticks([])
        axe.set_yticks([])
    axis[3].set_xlim(0, GENERATIONS)

    topfitness = []
    avefitness = []
    genaxis = []

    axis[0].imshow(BITMAP, cmap=COLORS)

    pop = toolbox.population(n=1000)
    s = deap.tools.Statistics()
    s.register("max", max)
    s.register("mean", np.mean)

    fit = 0.0
    counter = 0
    while (fit <= 1.0):

        opt_pop, log = deap.algorithms.eaMuPlusLambda(
            pop, toolbox,
            500, 500,  # 400, 100,  # parents, children
            .7, .3,    # .2, .4,  probabilities of cross or mut
            1,
            stats=s,
            verbose=False)         # iterations

        top = sorted(pop, key=lambda x: x.fitness.values[0])[-1]
        fit = top.fitness.values[0]

        top_entry, avg_entry = log.select("mean")
        topfitness.append(top_entry)
        avefitness.append(avg_entry)
        genaxis.append(counter)

        if counter % 10 == 0:
            # print(express(log.select("max")[-1]))
            print("Count: {} Mean: {}".format(counter, avg_entry))
            # print(counter, new_pop.best_individual().fitness, new_pop.total_fitness()/new_pop.size())
            axis[1].imshow(express(log.select("max")[-1]), cmap=COLORS)
            # axis[2].imshow(new_pop.mean_individual().express(), cmap=COLORS)
            axis[3].plot(genaxis[-11:], topfitness[-11:], color='blue')
            axis[3].plot(genaxis[-11:], avefitness[-11:], color='black')
            # axis[3].plot(genaxis[-11:], topfitness[-11:], color='blue')
            # axis[3].plot(genaxis[-11:], avefitness[-11:], color='black')
            plt.pause(0.01)

        counter += 1
    axis[1].imshow(express(log.select("max")[-1]), cmap=COLORS)
    print("Done!")


if __name__ == "__main__":
    main()
    sleep(60)
