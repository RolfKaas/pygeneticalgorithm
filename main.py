# -*- coding: utf-8 -*-
''' A simple implementation of a genetic algorithm in Python '''

__author__ = "Christian Brinch"
__copyright__ = "Copyright 2018-2019"
__credits__ = ["Christian Brinch"]
__license__ = "AFL 3.0"
__version__ = "1.0"
__maintainer__ = "Christian Brinch"
__email__ = "cbri@dtu.dk"

import copy as cp
import itertools as it
import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt

# Parameters for the run
SPRITE = 'ninja'
GENE_LENGTH = 1  # Used for precision when genes are floating points
MUTATION_RATE = 0.02
POPSIZE = 500
GENERATIONS = 1500

if SPRITE == 'ninja':
    BITMAP = np.array([
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1]
    ])
    COLORS = 'gray_r'
elif SPRITE == 'mario':
    BITMAP = np.array([
        [0, 0, 0, 3, 3, 3, 3, 3, 0, 0, 0, 0],
        [0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0],
        [0, 0, 2, 2, 2, 1, 1, 6, 1, 0, 0, 0],
        [0, 2, 1, 2, 1, 1, 1, 6, 1, 1, 1, 0],
        [0, 2, 1, 2, 2, 1, 1, 1, 6, 1, 1, 1],
        [0, 2, 2, 1, 1, 1, 1, 6, 6, 6, 6, 0],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 5, 3, 3, 5, 0, 0, 0, 0],
        [0, 3, 3, 3, 5, 3, 3, 5, 3, 3, 3, 0],
        [3, 3, 3, 3, 5, 5, 5, 5, 3, 3, 3, 3],
        [1, 1, 3, 5, 4, 5, 5, 4, 5, 3, 1, 1],
        [1, 1, 1, 5, 5, 5, 5, 5, 5, 1, 1, 1],
        [1, 1, 5, 5, 5, 5, 5, 5, 5, 5, 1, 1],
        [0, 0, 5, 5, 5, 0, 0, 5, 5, 5, 0, 0],
        [0, 2, 2, 2, 0, 0, 0, 0, 2, 2, 2, 0],
        [2, 2, 2, 2, 0, 0, 0, 0, 2, 2, 2, 2]
    ])
    COLORS = colors.ListedColormap(
        ['white', 'wheat', 'sienna', 'red', 'yellow', 'blue', 'black'])

N_GENES = len(BITMAP.ravel())


class Individual(object):
    ''' A class containg a single individual '''

    def __init__(self, genome=None, skills=None):
        if genome:
            self.genes = genome
        else:
            # self.genes = [round(np.random.random(), GENE_LENGTH) for _ in range(N_GENES)]
            self.genes = [np.random.randint(0, np.max(BITMAP)+1) for _ in range(N_GENES)]
        if skills:
            self.skills = skills
        else:
            self.skills = {}
        self.fitness = self.calculate_fitness()

    def calculate_fitness(self):
        ''' Calculate the cost function for an individual '''
        return 1./(sum([pow(self.genes[i] - BITMAP.ravel()[i], 2)
                        for i, _ in enumerate(BITMAP.ravel())])+1e-30)

    def express(self):
        ''' Reshape genome into a 2D array '''
        return np.reshape(self.genes, np.shape(BITMAP))

    def mutate(self):
        ''' Introduce a random mutation '''
        if np.random.random() < MUTATION_RATE:
            marker = np.random.randint(0, len(self.genes))
            # self.genes[marker] = round(np.random.random(), GENE_LENGTH)
            self.genes[marker] = np.random.randint(0, np.max(BITMAP)+1)

    def improve(self):
        ''' Improve the individual using a local heuristic '''
        idx = np.random.randint(0, np.max(BITMAP)+1)
        if self.genes[idx] != BITMAP.ravel()[idx]:
            self.genes[idx] = BITMAP.ravel()[idx]


class Population(object):
    ''' A class containg a whole population'''

    def __init__(self, pop_size=0):
        self.individuals = [Individual() for _ in range(pop_size)]

    def size(self):
        ''' Return size of population '''
        return len(self.individuals)

    def total_fitness(self):
        ''' Calculate the total fitness of populaition '''
        return sum([i.fitness for i in self.individuals])

    def best_individual(self):
        ''' Return the individual with highest fitness '''
        best = 0.
        alpha = None
        for individual in self.individuals:
            if individual.fitness > best:
                best = individual.fitness
                alpha = individual
        return alpha

    def mean_individual(self):
        ''' Return the average individual '''
        genomes = [individual.genes for individual in self.individuals]
        genome = list(np.median(genomes, axis=0))
        return Individual(genome)

    def natural_selection(self):
        ''' Perform Darwinian selection '''
        total_fitness = self.total_fitness()
        probs = [i.fitness/total_fitness for i in self.individuals]
        choice = np.random.choice(len(probs), p=probs, size=2, replace=False)
        return tuple([self.individuals[i] for i in choice])


def breed(parrents, method='uniform'):
    ''' Breed two individuals and return two offspring.'''
    offspring = []
    if method == 'single':
        # single point crossover
        marker = np.random.randint(0, N_GENES)
        for i in it.permutations([0, 1]):
            offspring.append(Individual(
                parrents[i[0]].genes[0:marker]+parrents[i[1]].genes[marker:]))
    elif method == 'dual':
        # two-point crossover
        marker1 = np.random.randint(0, N_GENES)
        marker2 = np.random.randint(marker1, N_GENES)
        for i in it.permutations([0, 1]):
            offspring.append(Individual(parrents[i[0]].genes[0:marker1] +
                                        parrents[i[1]].genes[marker1:marker2] +
                                        parrents[i[0]].genes[marker2:]))
    elif method == 'uniform':
        # Uniform crossover
        thresshold = 0.7
        probs = [np.random.random() for _ in range(N_GENES)]
        for j in it.permutations([0, 1]):
            child = [parrents[j[0]].genes[i] if probs[i] < thresshold else parrents[j[1]].genes[i]
                     for i in range(N_GENES)]
            offspring.append(Individual(child))
    elif method == 'flat':
        # Flat crossover -- poor performance
        for j in it.permutations([0, 1]):
            probs = [np.random.random() for _ in range(N_GENES)]
            child = [parrents[j[0]].genes[i]*probs[i]+parrents[j[1]].genes[i]
                     * (1-probs[i]) for i in range(N_GENES)]
            offspring.append(Individual(child))

    return offspring


def main():
    ''' First define a plot window, then enter main loop '''

    fig = plt.figure(1)
    plt.clf()
    axis = [fig.add_subplot(2, 3, c) for c in [1, 2, 3]]
    axis.append(fig.add_subplot(2, 3, 5))
    for axe in axis[:-1]:
        axe.set_xticks([])
        axe.set_yticks([])
    axis[3].set_xlim(0, GENERATIONS)

    topfitness = []
    avefitness = []
    genaxis = []

    axis[0].imshow(BITMAP, cmap=COLORS)

    new_pop = Population(POPSIZE)

    for gen in range(GENERATIONS):
        pop = cp.deepcopy(new_pop)
        new_pop = Population()

        # Elitism
        new_pop.individuals.append(pop.best_individual())
        while new_pop.size() < POPSIZE:
            parrents = pop.natural_selection()
            for child in breed(parrents, 'uniform'):
                child.mutate()
                new_pop.individuals.append(child)

        # Memetic heuristic
        # for individual in new_pop.individuals:
        #    individual.improve()

        topfitness.append(new_pop.best_individual().fitness)
        avefitness.append(new_pop.total_fitness()/new_pop.size())
        genaxis.append(gen)

        # Print some verbose info to the terminal
        if gen % 10 == 0:
            print(gen, new_pop.best_individual().fitness, new_pop.total_fitness()/new_pop.size())
            axis[1].imshow(new_pop.best_individual().express(), cmap=COLORS)
            axis[2].imshow(new_pop.mean_individual().express(), cmap=COLORS)
            axis[3].plot(genaxis[-11:], topfitness[-11:], color='blue')
            axis[3].plot(genaxis[-11:], avefitness[-11:], color='black')
            plt.pause(0.01)
            if new_pop.best_individual().fitness > 1.:
                break


if __name__ == "__main__":
    main()
